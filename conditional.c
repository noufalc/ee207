/* Conditional operators return true_value if condition is true and returns
 * false_value if condition is false. It has the form:
 *              (Condition)? true_value: false_value;
 */
#include<stdio.h>
main() 
{
    int x = 35, y = 55, max;
    max = (x > y) ? x : y;
    printf("Maximum = %d\n", max);

    /*TODO
     * Re-write the program to print the product of x and y if x < y, otherwise
     * print the value x/y.
     */
}
