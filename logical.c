/* Logical operators in C are
 *              +----------+-------------+
 *              | Operator |   Meaning   |
 *              +----------+-------------+
 *              |    &&    | logical AND |
 *              +----------+-------------+
 *              |    ||    |  logical OR |
 *              +----------+-------------+
 *              |     !    | logical NOT |
 *              +----------+-------------+
 */

#include <stdio.h>
main()
{
    int a = 5, b = 5, c = 10, result;
    printf("a = %d, b = %d, c = %d\n", a, b, c);


    result = (a == b) && (c > b);
    printf("(a == b) && (c > b) equals to %d \n", result);
    /*TODO
     * verify the operation of all other logical operators
     */

}
