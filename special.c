/* sizeof operator is used to evaluate the size of a variable.
 * It can be applied to a data type(eg int) or a variable(eg:- b) in the 
 * following program.
 */

{
    char b = 8;
    printf("Size of int: %d bytes\n",sizeof(int));
    printf("Size of b: %d bytes\n",sizeof(b));
    /* TODO
     * modify the program to print size of all other datatypes
     */
}
