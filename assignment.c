/* The '=' character is known as the assignmet operator. It will assign the
 * value/expression in the RHS to the variable in the LHS. C can also have certain
 * 'shorthand assignment operators of the form
 *                  variable operator= value/expression;
 *  where operator is any arithmetic operator.
 */


main()
{
    int a = 5, c;

    c = a;
    printf("c = %d \n", c);

    c += a; 
    printf("c = %d \n", c);
    
    /*TODO
     * Try all other shorthand operators using other arithmetic operators
     */
}
