/* This is a basic c program. This program prints the message on the standard
 * output device or stdout, ie., monitor.  Printf function is used to print
 * a sequence of characters on the stdout. This function is a part of the
 * standard input-output library. So we need to include the header 
 * file(.h file) in the beginnig using the '#include' preprocessor directive.
 */
#include <stdio.h>
main() 
{
    printf("Hello world\n");
}
