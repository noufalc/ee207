/* The bitwise operators availabe in C are
 *          +----------+------------------+
 *          | Operator |      Meaning     |
 *          +----------+------------------+
 *          |     &    |    bitwise and   |
 *          +----------+------------------+
 *          |     |    |    bitwise or    |
 *          +----------+------------------+
 *          |     ~    | one's complement |
 *          +----------+------------------+
 *          |     ^    |    bitwise xor   |
 *          +----------+------------------+
 *          |    <<    |    left shift    |
 *          +----------+------------------+
 *          |    >>    |    right shift   |
 *          +----------+------------------+
 * Bitwise operators may not appled with float or double types
 */

#include <stdio.h>
main()
{
    int a = 12, b = 25;

    printf("a & b = %d\n", a&b);

    /*TODO
     * Try all other shorthand operators using other arithmetic operators
     */
}
