/*  Relational operators available in C are
 *          +----------+-----------------------------+
 *          | Operator |           Meaning           |
 *          +----------+-----------------------------+
 *          |     >    |       is greater thatn      |
 *          +----------+-----------------------------+
 *          |    >=    | is greater than or equal to |
 *          +----------+-----------------------------+
 *          |     <    |         is less than        |
 *          +----------+-----------------------------+
 *          |    <=    |   is less than or equal to  |
 *          +----------+-----------------------------+
 *          |    ==    |         is equal to         |
 *          +----------+-----------------------------+
 *          |    !=    |       is not equal to       |
 *          +----------+-----------------------------+
 *
 *  when a relation expression evaluates to TRUE, the result of that
 *  expression will be 1, if the expression evaluates to FALSE, the result of
 *  that expression will be 0
 */
#include <stdio.h>
main()
{
    int a = 5, b = 5;

    printf("%d == %d = %d \n", a, b, a == b); 
    printf("%d == %d = %d \n", a, c, a == c);

    /*TODO
     * Verify the working of all other relation operator, and correct the given
     * code if any part is missing.
     */
}
