/* '++' and '--' are increment and decrement operators respectively.
 * It increment and decrement the value by 1. We can either prefix or postfix
 * the operator. These two forms have different values when used in expression.
 * The expression ++n increments n before its values is used, and n++ increments
 * n after its value has been used.
 */
#include <stdio.h>
main()
{
    int a = 10, b = 100, c;

    printf("a = %d\n", a);
    printf("b = %d\n", b);

    a++;
    printf("a = %d\n", a);

    b--;
    printf("b = %d\n", b);

    c = a++;
    printf("a = %d\tc = %d\n", a, c);

    c = ++a;
    printf("a = %d\tc = %d\n", a, c);

    c = b--;
    printf("b = %d\tc = %d\n", b, c);

    c = --b;
    printf("b = %d\tc = %d\n", b, c);
}
