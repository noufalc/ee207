/* Arithmetic operators in C are
 *      +       addition
 *      -       subraction
 *      *       multiplication
 *      /       division
 *      %       modulo division (mod); not applicable to float or double
 */

#include <stdio.h>
main()
{
    int a = 9,b = 4, c;
    printf("a = %d \n",a);
    printf("b = %d \n",b);
    c = a+b;
    printf("a+b = %d \n",c);

    /*TODO
     * verify the operation of all other arithmetic operators
     */
}
